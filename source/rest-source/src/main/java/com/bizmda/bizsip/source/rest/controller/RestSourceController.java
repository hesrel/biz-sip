package com.bizmda.bizsip.source.rest.controller;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.bizmda.bizsip.source.Source;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;

/**
 * @author 史正烨
 */
@Slf4j
@RestController
public class RestSourceController {
    @Value(("${bizsip.source-id}"))
    private String sourceId;

    @Autowired
    private Source source;

    @PostConstruct
    public void init() {
        try {
            this.source.init(this.sourceId);
        } catch (BizException e) {
            log.error("Source服务接入端初始化失败！",e);
        }
    }

    @PostMapping(value = "/rest", consumes = "application/json", produces = "application/json")
    public Object doService(@RequestBody JSONObject inMessage, HttpServletResponse response) {
        Object outMessage = null;
        try {
            outMessage = this.source.process(BizUtils.getBytes(inMessage.toString()));
            return outMessage;
        } catch (BizException e) {
            return JSONUtil.createObj()
                    .set("code", e.getCode())
                    .set("message", e.getMessage())
                    .set("extMessage", e.getExtMessage());
        }
    }
}