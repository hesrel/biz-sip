package com.bizmda.bizsip.source.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 史正烨
 */
@Slf4j
@SpringBootApplication
public class RestSourceApplication {
    public static void main(String[] args) {
        SpringApplication.run(RestSourceApplication.class, args);
    }
}
