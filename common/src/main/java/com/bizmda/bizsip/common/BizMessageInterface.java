package com.bizmda.bizsip.common;

import cn.hutool.json.JSONObject;

public interface BizMessageInterface {
    public BizMessage<JSONObject> call(JSONObject inData) throws BizException;
}
