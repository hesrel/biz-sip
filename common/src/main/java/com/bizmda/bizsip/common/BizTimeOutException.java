package com.bizmda.bizsip.common;

public class BizTimeOutException extends BizException {
    public BizTimeOutException() {
        super(BizResultEnum.INTEGRATOR_SERVICE_TIMEOUT);
    }

    public BizTimeOutException(String extMessage) {
        super(BizResultEnum.INTEGRATOR_SERVICE_TIMEOUT, extMessage);
    }
}
