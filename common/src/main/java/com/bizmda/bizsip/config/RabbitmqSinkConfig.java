package com.bizmda.bizsip.config;

import lombok.Getter;

import java.util.Map;

/**
 * @author 史正烨
 */
@Getter
public class RabbitmqSinkConfig extends AbstractSinkConfig {
    private String routingKey;
    private String exchange;
    private boolean rpcMode;

    public RabbitmqSinkConfig(Map map) {
        super(map);
        this.setType("rabbitmq");
        this.routingKey = (String)map.get("routing-key");
        this.exchange = (String)map.get("exchange");
        Object o = map.get("rpc-mode");
        if (o instanceof Boolean) {
            this.rpcMode = (Boolean) map.get("rpc-mode");
        }
        else {
            this.rpcMode = false;
        }
    }
}
