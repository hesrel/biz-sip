package com.bizmda.bizsip.integrator.listener;

import cn.hutool.core.text.StrFormatter;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.bizmda.bizsip.common.*;
import com.bizmda.bizsip.integrator.config.IntegratorServiceMapping;
import com.bizmda.bizsip.integrator.config.RabbitmqConfig;
import com.bizmda.bizsip.integrator.executor.AbstractIntegratorExecutor;
import com.bizmda.bizsip.integrator.service.SipServiceLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * RabbitMQ接收服务
 */
@Slf4j
@Service
public class DelayServiceQueueListener {
    @Autowired
    private IntegratorServiceMapping integratorServiceMapping;
    @Autowired
    private SipServiceLogService sipServiceLogService;
    @Autowired
    private RabbitTemplate rabbitTemplate;
    private Jackson2JsonMessageConverter jackson2JsonMessageConverter =new Jackson2JsonMessageConverter();

    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = RabbitmqConfig.DELAY_SERVICE_QUEUE, durable = "true", autoDelete = "false"),
            exchange = @Exchange(value = RabbitmqConfig.DELAY_SERVICE_EXCHANGE, type = ExchangeTypes.DIRECT, durable = "true", autoDelete = "false"),
            key = RabbitmqConfig.DELAY_SERVICE_ROUTING_KEY))
//    @RabbitListener(queues = RabbitmqConfig.DELAY_SERVICE_QUEUE)
    public void doService(Message message) {
        Map<String,Object> map = (Map<String,Object>)jackson2JsonMessageConverter.fromMessage(message);
        String serviceId = (String)map.get("serviceId");
        int retryCount = (int)map.get("retryCount");
        List<Integer> delayMillisecondList = (List<Integer>)map.get("delayMilliseconds");
        if (delayMillisecondList == null) {
            delayMillisecondList = new ArrayList<>();
        }
        JSONObject jsonObject = (JSONObject)JSONUtil.parse(map.get("bizmessage"));
        BizMessage<JSONObject> inBizMessage = new BizMessage<>(jsonObject);
        log.debug("延迟服务队列收到消息({},{},{}),\n{}",retryCount,delayMillisecondList,serviceId,BizUtils.buildBizMessageLog(inBizMessage));
        BizMessage<JSONObject> outBizMessage;
        AbstractIntegratorExecutor integratorService = this.integratorServiceMapping.getIntegratorService(serviceId);
        if (integratorService == null) {
            log.error("延迟服务不存在:{}",serviceId);
            outBizMessage = BizMessage.buildFailMessage(inBizMessage
                    ,new BizException(BizResultEnum.INTEGRATOR_SERVICE_NOT_FOUND
                    ,StrFormatter.format("聚合服务不存在:{}",serviceId)));
            this.sipServiceLogService.sendFailLog(inBizMessage,outBizMessage);
            return;
        }
        TmContext tmContext = new TmContext();
        log.debug("重试次数+1:{}",retryCount);
        tmContext.setRetryCount(retryCount+1);

        BizUtils.tmContextThreadLocal.set(tmContext);
        BizUtils.bizMessageThreadLocal.set(inBizMessage);

        try {
            outBizMessage = integratorService.doBizService(inBizMessage);
            log.debug("Integrator返回:\n{}",BizUtils.buildJsonLog(outBizMessage));
        } catch (BizException e) {
            if (e.getCode() != BizResultEnum.INTEGRATOR_SERVICE_TIMEOUT.getCode()) {
                outBizMessage = BizMessage.buildFailMessage(inBizMessage, e);
                this.sipServiceLogService.sendFailLog(inBizMessage, outBizMessage);
                return;
            }
            // 聚合服务返回超时的处理
            retryCount ++;
            if (retryCount >= delayMillisecondList.size()) {
                // 重试次数超限
                outBizMessage = BizMessage.buildFailMessage(inBizMessage, new BizException(BizResultEnum.INTEGRATOR_SERVICE_MAXIMUM_RETRY_ERROR));
                this.sipServiceLogService.sendFailLog(inBizMessage, outBizMessage);
                return;
            }
            int delayMillisecond = delayMillisecondList.get(retryCount);
            map.put("retryCount",retryCount);
            this.rabbitTemplate.convertAndSend(RabbitmqConfig.DELAY_SERVICE_EXCHANGE, RabbitmqConfig.DELAY_SERVICE_ROUTING_KEY, map,
                    new MessagePostProcessor() {
                        @Override
                        public Message postProcessMessage(Message message) {
                            //设置消息持久化
                            message.getMessageProperties().setDeliveryMode(MessageDeliveryMode.PERSISTENT);
                            message.getMessageProperties().setDelay(delayMillisecond);
                            return message;
                        }
                    });
            return;
        } finally {
            tmContext = BizUtils.tmContextThreadLocal.get();
            BizUtils.tmContextThreadLocal.remove();
            BizUtils.bizMessageThreadLocal.remove();
        }

//        if (outBizMessage.getCode() != 0) {
//            outBizMessage = BizMessage.buildFailMessage(inBizMessage
//                    ,new BizException(BizResultEnum.INTEGRATOR_SCRIPT_RETURN_ERROR));
//            log.debug("保存服务失败日志");
//            this.sipServiceLogService.saveErrorServiceLog(inBizMessage,outBizMessage);
//            return;
//        }
        log.debug("保存服务成功日志");
        this.sipServiceLogService.sendSuccessLog(inBizMessage,outBizMessage);
        return;
    }
}
