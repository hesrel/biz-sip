package com.bizmda.bizsip.integrator.executor;

import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.common.BizUtils;
import com.bizmda.bizsip.integrator.service.AppBeanInterface;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
public class AppBeanIntegratorExecutor extends AbstractIntegratorExecutor {
    private AppBeanInterface appBeanService;

    public AppBeanIntegratorExecutor(String serviceId, String type, Map configMap) {
        super(serviceId, type, configMap);
        Class clazz;
        String className = (String)configMap.get("className");
        try {
            clazz = Class.forName(className);
        } catch (ClassNotFoundException e) {
            log.error("创建"+className+"类失败！",e);
            return;
        }
        Object object = SpringUtil.getBean(clazz);
        if (object instanceof AppBeanInterface) {
            this.appBeanService = (AppBeanInterface)object;
        }
        else {
            log.error("类"+className+"不是AppBeanInterface接口实现类，创建BizService服务失败");
        }
    }

    @Override
    public void init() {
        // 没有初始化内容
    }

    @Override
    public BizMessage<JSONObject> doBizService(BizMessage<JSONObject> bizMessage) throws BizException {
        BizUtils.debug("入参",bizMessage);
        log.debug("执行Java引擎:{}",this.appBeanService.getClass().getName());
        JSONObject jsonObject = this.appBeanService.process(bizMessage.getData());
        BizUtils.debug("函数返回",jsonObject);
        bizMessage.setData(jsonObject);
        return bizMessage;
    }
}
