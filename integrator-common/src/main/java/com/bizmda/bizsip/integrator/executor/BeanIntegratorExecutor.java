package com.bizmda.bizsip.integrator.executor;

import cn.hutool.core.util.ReflectUtil;
import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.common.BizResultEnum;
import com.bizmda.bizsip.common.BizUtils;
import com.bizmda.bizsip.integrator.service.IntegratorBeanInterface;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.Map;

@Slf4j
public class BeanIntegratorExecutor extends AbstractIntegratorExecutor {
    private IntegratorBeanInterface javaIntegratorService;
    private Class clazz;
    private Object springBean;

    public BeanIntegratorExecutor(String serviceId, String type, Map configMap) {
        super(serviceId, type, configMap);
        String className = (String)configMap.get("className");
        try {
            this.clazz = Class.forName(className);
        } catch (ClassNotFoundException e) {
            log.error("创建"+className+"类失败！",e);
            return;
        }
        this.springBean = SpringUtil.getBean(clazz);
    }

    @Override
    public void init() {
        // 没有初始化内容
    }

    @Override
    public BizMessage<JSONObject> doBizService(BizMessage<JSONObject> message) throws BizException {
        BizUtils.debug("入参",message);
        String methodName = (String)message.getData().get("methodName");
        Method method = ReflectUtil.getMethodByName(this.clazz,methodName);
        if (method == null) {
            throw new BizException(BizResultEnum.INTEGRATOR_SPRINGBEAN_METHOD_NOT_FOUND,this.clazz.getName()+"的方法:" +methodName);
        }
        Object[] args = BizUtils.JsonObject2MethodParameters(method,message.getData().get("params"),(JSONObject)message.getData().get("paramsTypes"));
        Object returnValue = null;
        try {
            returnValue = method.invoke(this.springBean,args);
        } catch (IllegalAccessException e) {
            log.error("执行方法出错",e);
            throw new BizException(BizResultEnum.OTHER_JAVA_CLASS_METHOD_ERROR,e);
        } catch (InvocationTargetException e) {
//            UndeclaredThrowableException e1;
//            e1.getUndeclaredThrowable()
//            java.lang.reflect.UndeclaredThrowableException
            Throwable t = e.getTargetException();
            if (t instanceof BizException) {
                throw (BizException) t;
            }
            else if (t instanceof UndeclaredThrowableException) {
                Throwable t1 = ((UndeclaredThrowableException) t).getUndeclaredThrowable();
                if (t1 instanceof BizException) {
                    throw (BizException) t1;
                }
            }

            log.error("执行方法出错",e);
            throw new BizException(BizResultEnum.OTHER_JAVA_CLASS_METHOD_ERROR,e);
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.set("result", BizUtils.methodReturnValue2Json(returnValue));
        BizMessage<JSONObject> result =  BizMessage.buildJSONObjectMessage(message,jsonObject);
        BizUtils.debug("函数返回",result);
        return result;
    }
}
