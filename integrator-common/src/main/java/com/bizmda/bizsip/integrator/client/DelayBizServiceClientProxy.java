package com.bizmda.bizsip.integrator.client;

import lombok.Getter;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

@Getter
public class DelayBizServiceClientProxy<T> implements InvocationHandler, Serializable {

    private final Class<T> mapperInterface;
    private final Map<Method, DelayBizServiceClientMethod> methodCache;
    private String bizServiceId;
    private int[] delayMilliseconds;

    public DelayBizServiceClientProxy(Class<T> mapperInterface, String bizServiceId, int[] delayMilliseconds) {
        this.mapperInterface = mapperInterface;
        this.methodCache = new HashMap<>();
        this.bizServiceId = bizServiceId;
        this.delayMilliseconds = delayMilliseconds;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (Object.class.equals(method.getDeclaringClass())) {
            try {
                return method.invoke(this, args);
            } catch (Throwable t) {
                throw t;
            }
        }
        final DelayBizServiceClientMethod delayBizServiceClientMethod = cachedMapperMethod(method);

        return delayBizServiceClientMethod.execute(args);
    }

    private DelayBizServiceClientMethod cachedMapperMethod(Method method) {
        DelayBizServiceClientMethod delayBizServiceClientMethod = methodCache.get(method);
        if (delayBizServiceClientMethod == null) {
            delayBizServiceClientMethod = new DelayBizServiceClientMethod(method, this);
            methodCache.put(method, delayBizServiceClientMethod);
        }
        return delayBizServiceClientMethod;
    }

}
