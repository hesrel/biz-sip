package com.bizmda.bizsip.integrator.executor;

import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.common.BizUtils;
import com.bizmda.bizsip.integrator.service.IntegratorBeanInterface;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
public class IntegratorBeanIntegratorExecutor extends AbstractIntegratorExecutor {
    private IntegratorBeanInterface javaIntegratorService;

    public IntegratorBeanIntegratorExecutor(String serviceId, String type, Map configMap) {
        super(serviceId, type, configMap);
        Class clazz;
        String className = (String)configMap.get("className");
        try {
            clazz = Class.forName(className);
        } catch (ClassNotFoundException e) {
            log.error("创建"+className+"类失败！",e);
            return;
        }
        Object object = SpringUtil.getBean(clazz);
        if (object instanceof IntegratorBeanInterface) {
            this.javaIntegratorService = (IntegratorBeanInterface)object;
        }
        else {
            log.error("类"+className+"不是BizServiceInterface接口实现类，创建BizService服务失败");
        }
    }

    @Override
    public void init() {
        // 没有初始化内容
    }

    @Override
    public BizMessage<JSONObject> doBizService(BizMessage<JSONObject> message) throws BizException {
        BizUtils.debug("入参",message);
        log.debug("执行Java引擎:{}",this.javaIntegratorService.getClass().getName());
        BizMessage<JSONObject> result = this.javaIntegratorService.doBizService(message);
        BizUtils.debug("函数返回",result);
        return result;
    }
}
