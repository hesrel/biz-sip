package com.bizmda.bizsip.sample.source.controller;

import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.sample.sink.api.AccountDTO;
import com.bizmda.bizsip.sample.sink.api.CustomerDTO;
import com.bizmda.bizsip.sample.sink.api.SinkInterface1;
import com.bizmda.bizsip.source.client.SourceClientFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * @author 史正烨
 */
@Slf4j
@RestController
public class SampleSource2Controller implements SinkInterface1 {
    private SinkInterface1 bizServiceClient = SourceClientFactory
            .getBizServiceClient(SinkInterface1.class,"/springbean");

    private SinkInterface1 sink12Interface =  SourceClientFactory
            .getBizServiceClient(SinkInterface1.class,"/sink12");;

    @Override
    @GetMapping(value ="/doService1")
    public String doService1(@RequestParam("arg1") String arg1) {
        return this.bizServiceClient.doService1(arg1);
    }

    @Override
    @GetMapping(value ="/doService2")
    public void doService2(@RequestParam("arg1") String arg1,@RequestParam("arg2")  int arg2) {
        this.bizServiceClient.doService2(arg1,arg2);
    }

    @Override
    @GetMapping(value ="/doService1Exception")
    public String doService1Exception(@RequestParam("arg1") String arg1) {
        try {
            return this.bizServiceClient.doService1Exception(arg1);
        } catch (BizException e) {
            e.printStackTrace();
            return e.getCode()+","+e.getMessage();
        }
    }

    @GetMapping(value ="/doSink12Service1Exception")
    public String doSink12Service1Exception(@RequestParam("arg1") String arg1) {
        try {
            return this.sink12Interface.doService1Exception(arg1);
        } catch (BizException e) {
            e.printStackTrace();
            return e.getCode()+","+e.getMessage();
        }
    }

    @Override
    @GetMapping(value ="/queryCustomerDTO")
    public CustomerDTO queryCustomerDTO(@RequestParam("customerId") String customerId) {
        return this.bizServiceClient.queryCustomerDTO(customerId);
    }

    @Override
    @PostMapping(value ="/queryAccounts")
    public AccountDTO[] queryAccounts(@RequestBody AccountDTO accountDTO) {
        return this.bizServiceClient.queryAccounts(accountDTO);
    }

    @Override
    @PostMapping(value ="/queryAccountList")
    public List<AccountDTO> queryAccountList(@RequestBody AccountDTO accountDTO) {
        return this.bizServiceClient.queryAccountList(accountDTO);
    }

    @Override
    public String notify(int maxRetryNum, String result) throws BizException {
        return null;
    }

    @Override
    @PostMapping(value ="/saveAll")
    public void saveAll(@RequestBody AccountDTO[] accountDTOS) {
        AccountDTO accountDTO = AccountDTO.builder().build();
        accountDTOS = this.bizServiceClient.queryAccounts(accountDTO);
        this.bizServiceClient.saveAll(accountDTOS);
        return;
    }

    @Override
    public void saveAllList(List<AccountDTO> accountDTOList) {

    }

    @Override
    public void testParamsType(Object arg1, Object arg2, Object arg3) {

    }
}