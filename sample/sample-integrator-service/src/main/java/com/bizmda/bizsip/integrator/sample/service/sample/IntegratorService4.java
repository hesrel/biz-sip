package com.bizmda.bizsip.integrator.sample.service.sample;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.integrator.service.AppBeanInterface;
import com.bizmda.bizsip.integrator.service.SipService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class IntegratorService4 implements AppBeanInterface {
    @Autowired
    private SipService sipService;

    @Override
    public JSONObject process(JSONObject message) throws BizException {
        this.sipService.doDelayService("/sample/delayservice1",message,
                1000,2000,4000,8000,16000,32000);
        return message;
    }
}
