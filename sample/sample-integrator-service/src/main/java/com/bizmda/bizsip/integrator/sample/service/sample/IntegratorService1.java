package com.bizmda.bizsip.integrator.sample.service.sample;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.common.BizMessageInterface;
import com.bizmda.bizsip.integrator.client.IntegratorClientFactory;
import com.bizmda.bizsip.integrator.service.AppBeanInterface;
import com.bizmda.bizsip.integrator.service.SipService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class IntegratorService1 implements AppBeanInterface {
    private BizMessageInterface sink1Interface = IntegratorClientFactory
            .getSinkClient(BizMessageInterface.class,"sink1");


    @Override
    public JSONObject process(JSONObject message) throws BizException {
        BizMessage<JSONObject> bizMessage = this.sink1Interface.call(message);
        return bizMessage.getData();
    }
}
