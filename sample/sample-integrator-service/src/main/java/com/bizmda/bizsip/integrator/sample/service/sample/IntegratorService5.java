package com.bizmda.bizsip.integrator.sample.service.sample;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.integrator.client.IntegratorClientFactory;
import com.bizmda.bizsip.integrator.service.AppBeanInterface;
import com.bizmda.bizsip.integrator.service.IntegratorBeanInterface;
import com.bizmda.bizsip.sample.sink.api.SinkInterface1;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Slf4j
@Service
public class IntegratorService5 implements AppBeanInterface {
    private SinkInterface1 delayServiceClient = IntegratorClientFactory
            .getDelayBizServiceClient(SinkInterface1.class,"/springbean",
            1000,2000,4000,8000,16000,32000);

    @Override
    public JSONObject process(JSONObject message) throws BizException {
        int maxRetryNum = (int)message.get("maxRetryNum");
        String result = (String)message.get("result");
        String result1 = this.delayServiceClient.notify(maxRetryNum,result);
        log.info("成功返回: {}",result1);
        return message;
    }
}
