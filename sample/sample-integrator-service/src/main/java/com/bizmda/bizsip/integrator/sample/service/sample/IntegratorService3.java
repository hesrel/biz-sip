package com.bizmda.bizsip.integrator.sample.service.sample;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.common.BizMessageInterface;
import com.bizmda.bizsip.integrator.client.IntegratorClientFactory;
import com.bizmda.bizsip.integrator.service.AppBeanInterface;
import com.bizmda.bizsip.sample.sink.api.AccountDTO;
import com.bizmda.bizsip.sample.sink.api.CustomerDTO;
import com.bizmda.bizsip.sample.sink.api.SinkInterface1;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class IntegratorService3 implements AppBeanInterface {
    private SinkInterface1 sinkInterface1 = IntegratorClientFactory
            .getSinkClient(SinkInterface1.class,"sink12");
    private BizMessageInterface sinkInterface2 = IntegratorClientFactory
            .getSinkClient(BizMessageInterface.class,"sink1");

    @Override
    public JSONObject process(JSONObject message) throws BizException {
        String result1 = this.sinkInterface1.doService1("001");
        this.sinkInterface1.doService2("002",3);
        AccountDTO[] accountDTOs = this.sinkInterface1.queryAccounts(AccountDTO.builder().account("002").build());
        List<AccountDTO> accountDTOList = this.sinkInterface1.queryAccountList(AccountDTO.builder().account("002").build());
        CustomerDTO customerDTO = this.sinkInterface1.queryCustomerDTO("001");
        this.sinkInterface1.saveAllList(accountDTOList);
        this.sinkInterface1.saveAll(accountDTOs);
        this.sinkInterface1.testParamsType(accountDTOList,null,customerDTO);
        JSONObject jsonObject1 = new JSONObject();
        jsonObject1.set("accountNo","003");
        BizMessage<JSONObject> bizMessage = this.sinkInterface2.call(jsonObject1);
        JSONObject jsonObject = new JSONObject();
        jsonObject.set("result1",result1);
        jsonObject.set("accountDTOs",accountDTOs);
        jsonObject.set("accountDTOList",accountDTOList);
        jsonObject.set("customerDTO",customerDTO);
        jsonObject.set("sink1",bizMessage.getData());
        return jsonObject;
    }
}
