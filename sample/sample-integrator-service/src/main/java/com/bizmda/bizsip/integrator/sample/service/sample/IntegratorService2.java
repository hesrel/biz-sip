package com.bizmda.bizsip.integrator.sample.service.sample;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.common.BizMessageInterface;
import com.bizmda.bizsip.integrator.client.IntegratorClientFactory;
import com.bizmda.bizsip.integrator.service.AppBeanInterface;
import com.bizmda.bizsip.integrator.service.IntegratorBeanInterface;
import com.bizmda.bizsip.integrator.service.SipService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class IntegratorService2 implements AppBeanInterface {
    private BizMessageInterface delayBizServiceClient = IntegratorClientFactory
            .getDelayBizServiceClient(BizMessageInterface.class,"/openapi/safservice1",1000,2000,4000);

    @Override
    public JSONObject process(JSONObject message) throws BizException {
        // 类似于"/openapi/safservice.script"
//        this.sipService.setTmDelayTime(1000);
        this.delayBizServiceClient.call(message);
        return message;
    }
}
