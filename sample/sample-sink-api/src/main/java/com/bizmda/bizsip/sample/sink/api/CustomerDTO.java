package com.bizmda.bizsip.sample.sink.api;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerDTO {
    private String customerId;
    private String name;
    private char sex;
    private int age;
    private boolean isMarried;
}
