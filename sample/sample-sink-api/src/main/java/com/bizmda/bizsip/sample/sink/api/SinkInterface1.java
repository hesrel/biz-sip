package com.bizmda.bizsip.sample.sink.api;

import com.bizmda.bizsip.common.BizException;

import java.util.List;

public interface SinkInterface1 {
    public String doService1(String arg1);
    public void doService2(String arg1,int arg2);
    public String doService1Exception(String arg1) throws BizException;
    public CustomerDTO queryCustomerDTO(String customerId);
    public AccountDTO[] queryAccounts(AccountDTO accountDTO);
    public List<AccountDTO> queryAccountList(AccountDTO accountDTO);
    public String notify(int maxRetryNum,String result) throws BizException;
    public void saveAll(AccountDTO[] accountDTOS);
    public void saveAllList(List<AccountDTO> accountDTOList);
    public void testParamsType(Object arg1,Object arg2,Object arg3);
}
