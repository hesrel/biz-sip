package com.bizmda.bizsip.sample.sink.api;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AccountDTO {
    private String account;
    private long balance;
}
