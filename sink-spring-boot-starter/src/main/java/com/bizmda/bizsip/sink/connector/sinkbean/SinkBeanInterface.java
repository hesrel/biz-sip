package com.bizmda.bizsip.sink.connector.sinkbean;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;

/**
 * @author 史正烨
 */
public interface SinkBeanInterface {
    /**
     * Java服务调用接口
     * @param packMessage 传入的消息
     * @return 返回值
     * @throws BizException
     */
    public byte[] process(byte[] packMessage) throws BizException;
}
