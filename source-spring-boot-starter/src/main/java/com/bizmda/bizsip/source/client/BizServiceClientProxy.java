package com.bizmda.bizsip.source.client;

import lombok.Getter;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

@Getter
public class BizServiceClientProxy<T> implements InvocationHandler, Serializable {

    private final Class<T> mapperInterface;
    private final Map<Method, BizServiceClientMethod> methodCache;
    private String bizServiceId;

    public BizServiceClientProxy(Class<T> mapperInterface, String bizServiceId) {
        this.mapperInterface = mapperInterface;
        this.methodCache = new HashMap<>();
        this.bizServiceId = bizServiceId;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (Object.class.equals(method.getDeclaringClass())) {
            try {
                return method.invoke(this, args);
            } catch (Throwable t) {

            }
        }
        final BizServiceClientMethod serviceClientMethod = cachedMapperMethod(method);

        return serviceClientMethod.execute(args);
    }

    private BizServiceClientMethod cachedMapperMethod(Method method) {
        BizServiceClientMethod bizServiceClientMethod = methodCache.get(method);
        if (bizServiceClientMethod == null) {
            bizServiceClientMethod = new BizServiceClientMethod(method, this);
            methodCache.put(method, bizServiceClientMethod);
        }
        return bizServiceClientMethod;
    }

}
