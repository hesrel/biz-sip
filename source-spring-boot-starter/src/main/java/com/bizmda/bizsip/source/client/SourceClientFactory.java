package com.bizmda.bizsip.source.client;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class SourceClientFactory<T> {
//    private final Class<T> clientInterface;
//    private final Map<Method, BizServiceClientMethod> methodCache = new ConcurrentHashMap<>();

//    public SourceClientFactory(Class<T> clientInterface) {
//        this.clientInterface = clientInterface;
//    }

//    public Class<T> getClientInterface() {
//        return clientInterface;
//    }

//    public Map<Method, BizServiceClientMethod> getMethodCache() {
//        return methodCache;
//    }

//    private T newInstance(BizServiceClientProxy<T> serviceClientProxy){
//        return (T) Proxy.newProxyInstance(clientInterface.getClassLoader(), new Class[]{clientInterface}, serviceClientProxy);
//    }
//
//    public T getBizServiceClient(String bizServiceId){
//        final BizServiceClientProxy<T> bizServiceClientProxy = new BizServiceClientProxy<>(clientInterface, methodCache, bizServiceId);
//        return newInstance(bizServiceClientProxy);
//    }

    public static <T> T getBizServiceClient(Class<T> tClass,String bizServiceId) {
        final BizServiceClientProxy<T> bizServiceClientProxy = new BizServiceClientProxy<>(tClass,bizServiceId);
        return (T) Proxy.newProxyInstance(tClass.getClassLoader(), new Class[]{tClass}, bizServiceClientProxy);
    }
}
