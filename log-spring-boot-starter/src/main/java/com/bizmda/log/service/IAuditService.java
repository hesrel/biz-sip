package com.bizmda.log.service;

import com.bizmda.log.model.Audit;

/**
 * 审计日志接口
 *
 * @author zlt
 * Blog: https://zlt2000.gitee.io
 * Github: https://github.com/zlt2000
 */
public interface IAuditService {
    void save(Audit audit);
}
